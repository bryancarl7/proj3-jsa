# proj3-JSA
Vocabulary anagrams game for primary school English language learners (ELL)

## Overview

A simple anagram game designed for English-language learning students in elementary and middle school. Students are presented with a list of vocabulary words (taken from a text file) and an anagram. The anagram is a jumble of some number of vocabulary words, randomly chosen. Students attempt to type words that can be created from the jumble. When a matching word is typed, it is added to a list of solved words. 

The vocabulary word list is fixed for one invocation of the server, so multiple students connected to the same server will see the same vocabulary list but may  have different anagrams.

## Authors 

Initial version by M Young; Docker version added by R Durairajan; to be revised by CIS 322 students. 

## Status

Updated by a CIS 322 Student who cannot seem to get the files properly uploaded in the correct fashion. 

## Functionality
Hopefully it is filled out to project specs. The first time I committed my files it seemed to only accept 
half of the files that were actually staged to commit, didn't realize until much later. Readme is updsted now,
and hopefully functionality is still there

